from tkinter import *
from tkinter.messagebox import showinfo

#login venster wordt aangeroepen
def toonVenster1():
    def close():
        subwindow.withdraw()

    subwindow = Toplevel(master=root)
    label = Label(master=subwindow, text='Haal hier uw gegevens', background='yellow', foreground='blue', font=('Times New Roman', 16, 'bold'), width=50, height=10)
    label.pack()

    entry1 = Entry(master=subwindow, background='white')
    entry1.pack(pady=10, padx=10)

#registreer venster wordt aangeroepen
def toonVenster2():
    def close():
        subwindow.withdraw()

    subwindow = Toplevel(master=root)
    label = Label(master=subwindow, text='Vul hier uw gegevens in', background='blue', foreground='white', font=('Times New Roman', 16, 'bold'), width=40, height=3)
    label.pack()

    top_frame = Frame(master=subwindow)
    top_frame.pack(side=TOP)

    middle_frame = Frame(master=subwindow)
    middle_frame.pack()

    bottom_frame = Frame(master=subwindow)
    bottom_frame.pack(side=BOTTOM)

    entry1 = Entry(top_frame, background='white')
    label = Label(top_frame, text='Naam:                    ')
    label.pack(side=LEFT)
    entry1.pack(pady=10, padx=10, side=RIGHT)

    entry2 = Entry(middle_frame, background='white')
    label = Label(middle_frame, text='Emailadres:            ')
    label.pack(side=LEFT)
    entry2.pack(pady=10, padx=10, side=RIGHT)

    entry3 = Entry(bottom_frame, background='white')
    label = Label(bottom_frame, text='Telefoonnummer:')
    label.pack(side=LEFT)
    entry3.pack(pady=10, padx=10, side=RIGHT)

#Stallen scherm wordt aangeroepen
def toonVenster3():
    def close():
        subwindow.withdraw()
    subwindow = Toplevel(master=root)

    label = Label(master=subwindow, text='Vul hier uw gegevens in', background='blue', foreground='white', font=('Times New Roman', 16, 'bold'), width=40, height=3)
    label.pack()

    top_frame = Frame(master=subwindow)
    top_frame.pack(side=TOP)

    middle_frame = Frame(master=subwindow)
    middle_frame.pack()

    bottom_frame = Frame(master=subwindow)
    bottom_frame.pack(side=BOTTOM)

    entry1 = Entry(top_frame, background='white')
    label = Label(top_frame, text='Naam:                    ')
    label.pack(side=LEFT)
    entry1.pack(pady=10, padx=10, side=RIGHT)

    entry2 = Entry(middle_frame, background='white')
    label = Label(middle_frame, text='Fietscode:              ')
    label.pack(side=LEFT)
    entry2.pack(pady=10, padx=10, side=RIGHT)

#Ophalen scherm wordt aangeroepen
def toonVenster4():
    def close():
        subwindow.withdraw()
    subwindow = Toplevel(master=root)

    label = Label(master=subwindow, text='Vul hier uw gegevens in', background='blue', foreground='white', font=('Times New Roman', 16, 'bold'), width=40, height=3)
    label.pack()

    top_frame = Frame(master=subwindow)
    top_frame.pack(side=TOP)

    middle_frame = Frame(master=subwindow)
    middle_frame.pack()

    bottom_frame = Frame(master=subwindow)
    bottom_frame.pack(side=BOTTOM)

    entry1 = Entry(top_frame, background='white')
    label = Label(top_frame, text='Naam:                    ')
    label.pack(side=LEFT)
    entry1.pack(pady=10, padx=10, side=RIGHT)

    entry2 = Entry(middle_frame, background='white')
    label = Label(middle_frame, text='Fietscode:              ')
    label.pack(side=LEFT)
    entry2.pack(pady=10, padx=10, side=RIGHT)




root = Tk()
root.config(bg='#0000ff')

photo1 = PhotoImage(file="Logo_NS.png")
plaatje_ns = Label(root, image=photo1)
plaatje_ns.grid(row=0, column=1)

photo2 = PhotoImage(file="fiets.png")
plaatje_ns = Label(root, image=photo2)
plaatje_ns.grid(row=0, column=1)

#label
label = Label(master=root, text='Welkom bij de NS-Fietsenstalling', background='yellow', foreground='blue', font=('Times New Roman', 16, 'bold'), width=33, height=3)
label.grid(row=1, columnspan=3)

#ophalen button
button1 = Button(master=root, text='Ophalen', background='yellow', foreground='black', font=('Times New Roman', 11, 'bold'), command=toonVenster4)
# button1.config(anchor=W)
button1.grid(row=2, column=0)

#registeren button
button2 = Button(master=root, text='Registreren', background='yellow', foreground='black', font=('Times New Roman', 11, 'bold'), command=toonVenster2)
# button2.config(anchor=CENTER)
button2.grid(row=2, column=1)

#stallen button
button3 = Button(master=root, text='Stallen', background='yellow', foreground='black', font=('Times New Roman', 11, 'bold'), command=toonVenster3)
# button3.config(anchor=E)
button3.grid(row=2, column=2)


root.mainloop()
